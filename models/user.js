var mongoose = require('mongoose');

var UserSchema = new mongoose.Schema({
    name: String,
    email: String,
    password: String,
    google : {
        email: String,
        token: String
    }
});

let UsersCollection = mongoose.model('userscollection', UserSchema, 'userscollection')

module.exports = UsersCollection