const express = require('express');
const cors = require('cors');
const app = express();

app.use(cors())

const bodyParser = require('body-parser');

var usersRoute = require('./controllers/user.controller');

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));

app.use("/", usersRoute.getAllUsers)  // GET
app.use("/", usersRoute.createUser)     // POST
app.use("/", usersRoute.login)  //POST
app.use("/", usersRoute.updateProfileByName) // PUT
app.use("/", usersRoute.getProfileByName)  // GET 
app.use("/", usersRoute.deleteAccount)

// Google Auth
app.use("/", usersRoute.googleAuth)
app.use("/", usersRoute.googleAuthCallback)
app.use("/", usersRoute.fbAuth)
app.use("/", usersRoute.fbAuthCallback)

app.listen(process.env.PORT || 8000, () => {
    console.log("Server started on port 8000")
})
