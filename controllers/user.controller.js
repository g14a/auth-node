var bcrypt = require('bcrypt')
var express = require('express');
var dbConfig = require('../config/dbConfig');
var jwt = require('jsonwebtoken')
var passport = require('passport');
var GoogleAuthStrategy = require('passport-google-oauth').OAuth2Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var request = require('request');

var router = express.Router();

var User = require('../models/user');

var authValidationURL = "https://www.googleapis.com/oauth2/v1/tokeninfo";

var createUser = router.post('/register', (req, res) => {

    User.findOne({email: req.body.email}, (err, user) => {
        if (!user) {
            var hashPassword = bcrypt.hashSync(req.body.password, 10)

                User.create({
                    name: req.body.name,
                    email: req.body.email,
                    password: hashPassword,
                    google: {
                        email: "",
                        token: ""
                    }
                }, (err, user) => {
                    if (err)
                        return res.status(500).send("Could not create a new user, try again")
            
                    res.status(200).send(JSON.parse('{"message": "Created user successfully"}'))
                })     
            
        } else {
            return res.status(409).send("User already exists")
        }
    })
})

var login = router.post("/login", (req, res) => {

    User.findOne({email: req.body.email}, (err, user) => {
        if (err) {
            return res.status(500).send(err)
        }
        if (user) {
            var dbPassword = user['password'];
            if (bcrypt.compareSync(req.body.password, dbPassword)) {
                    const payload = {
                        name: user['name'],
                        email: req.body.email
                    }

                    var token = jwt.sign(payload, dbConfig.token, {
                        expiresIn: "2d"
                    })

                    res.status(200).send({
                        token
                    })
            } else {
                res.send("Invalid username or password")
            }
        }
    })
})

var getAllUsers = router.get("/", (req, res) => {
    User.find({}, (err, users) => {
        if (err)
            return res.status(500).send("Couldn't find all the users");

        res.status(200).send(users)
    })
})

// UPDATES profile's email by email
var updateProfileByName = router.put("/profile/:name", (req, res) => {

    var decoded = jwt.verify(req.headers['authorization'], dbConfig.token)

    User.updateOne({name: decoded.name}, {email: req.body.email}, (err, user) => {
        if (err)
            return res.status(500).send(JSON.parse('{"message": "Could not update the email"}'))

            res.status(200).send(JSON.parse('{"message": "Successully Modified"}'))
    })
})

// GETS profile by name
var getProfileByName = router.get("/profile", (req, res) => {

    var decoded = jwt.verify(req.headers['authorization'], dbConfig.token)

    User.findOne({name: decoded.name}, 'name email -_id', (err, user) => {
        if(err)
            return res.status(404).send("Could not find the profile")
        
        res.status(200).send(user)
    })
})

var deleteAccount = router.delete("/profile", (req, res) => {
    var decoded = jwt.verify(req.headers['authorization'], dbConfig.token)

    User.deleteOne({name: decoded.name}, (err) => {
        if(err)
            return res.status(500).send("Couldn't delete the user")

        res.send(JSON.parse('{"message": "Deleted"}'))
    })
})

// Google Auth Strategy
passport.use('google', new GoogleAuthStrategy( {
    clientID: "186233218589-regaofd39bljgih85mev0g9sj24pd4ov.apps.googleusercontent.com",
    clientSecret: "2xCo19zy6NP5y2aOiAm7FcxD",
    callbackURL: "https://node-scapic.herokuapp.com/auth/google/callback"
}, function(token, tokenSecret, profile, done) {
    console.log("Token: " + token);


    // User.find({'google.token': token}, (err, user) => {
    //     if (err)
    //         throw err;
        
    //     console.log(user)
    // })

    request.post(authValidationURL, {
        json: {
            access_token: token
        }
    }, (error, res, body) => {
        if(error) {
            console.log(error)
            return
        }
        console.log(res)
    })

        // User.create({
        //     name: profile.displayName,
        //     email: "",
        //     password: "",
        //     google: {   
        //         email: profile.emails[0].value,
        //         token: token
        //     }
        // }, (err, user) => {
        //     if (err)
        //         return res.status(500).send("Could not create a new user, try again")

        //     res.status(200).send(JSON.parse('{"message": "Created user successfully"}'))
        // })
    done()
}))

var googleAuth = router.post('/auth/google', 
                passport.authenticate('google', { scope: ['https://www.googleapis.com/auth/userinfo.profile', 'https://www.googleapis.com/auth/userinfo.email'], session: false}))

var googleAuthCallback = router.get('/auth/google/callback', 
                passport.authenticate('google', { failureRedirect: '/' }),
                function(req, res) {
                res.header("Access-Control-Allow-Origin", req.headers.origin);
                console.log(req);
                  res.redirect('/');
                });

// Facebook Auth
passport.use('facebook', new FacebookStrategy({
    clientID: "2008424652796654",
    clientSecret: "6eceed9cfc62e3f95098eb09c1846654",
    callbackURL: "http://88a4717e.ngrok.io/auth/facebook/callback",
    profileURL: 'https://graph.facebook.com/v2.10/me',
    authorizationURL: 'https://www.facebook.com/v2.10/dialog/oauth',
    tokenURL: 'https://graph.facebook.com/v2.10/oauth/access_token',
    profileFields: ['id', 'displayName', 'picture.width(200).height(200)', 'first_name', 'middle_name', 'last_name', 'gender', 'link', 'email', 'location', 'friends']
}, async (accessToken, refreshToken, profile, done) => {
    try {
        console.log('accesstoken', accessToken)
        console.log('refreshToken', refreshToken)
        console.log('profile', profile)
    } catch (err) {
        console.log(err)
        done(err, false, error.message)
    }
}))

var fbAuth = router.post('/auth/facebook', passport.authenticate('facebook', { scope: ['user_location', 'email', 'user_friends'] }))

var fbAuthCallback = router.get('/auth/facebook/callback', 
                passport.authenticate('facebook', { failureRedirect: '/' }),
                function(req, res) {
                console.log(req);
                  res.redirect('/');
                });


module.exports = {
    getAllUsers: getAllUsers,
    createUser: createUser,
    login: login,
    updateProfileByName:  updateProfileByName,
    getProfileByName: getProfileByName,
    deleteAccount: deleteAccount,
    googleAuth: googleAuth,
    googleAuthCallback: googleAuthCallback,
    fbAuth: fbAuth,
    fbAuthCallback: fbAuthCallback
}
